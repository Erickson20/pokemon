'use strict'
const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const EntrenadorSchema = Schema({
    user: {type: String, unique: true, lowercase: true},
    name: {type: String },
    img: {type: String},
    password: {type: String , select: false},
    Date: {type: Date, default: Date.now()}

})



module.exports = mongoose.model('Entrenador', EntrenadorSchema)