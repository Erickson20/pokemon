'use strict'
const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const PokemonSchema = Schema({
    name: {type: String, lowercase: true},
    type: {type: String },
    img: {type: String},
    level: {type: Number },
    DateCatch: {type: Date, default: Date.now()},
    Entrenador: { type: Schema.ObjectId, ref: "Entrenador" } 

})



module.exports = mongoose.model('Pokemon', PokemonSchema)