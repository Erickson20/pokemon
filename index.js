'use strict'

const mongoose = require('mongoose')
const config = require('./config')
const app = require('./app');

mongoose.connect(config.db, {useNewUrlParser: true}, (err, res) =>{
if(err){
    console.log('conexion no exitosa')
}else {
    app.listen(config.port);
    console.log('conexion exitosa en: '+config.port)
}
})