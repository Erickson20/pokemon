'use strict'

const express = require('express');
const entrenador = require('../controllers/entrenador');
const pokemon = require('../controllers/pokemon');
const auth = require('../middleware/auth')
const user = require('../controllers/auth');
const api = express.Router();


//api /entrenador

api.get('/entrenador/:id', entrenador.getEntrenador)
api.get('/entrenador', entrenador.getAllEntrenador)
api.post('/entrenador', entrenador.addEntrenador)
api.delete('/entrenador/:id', entrenador.deleteEntrenador)
api.put('/entrenador/:id', entrenador.updateEntrenador)

//api /pokemon

api.get('/pokemon/:id', pokemon.getPokemon)
api.get('/pokemon', pokemon.getAllPokemons)
api.get('/pokemonEnd', pokemon.getEndPokemons)
api.post('/pokemon', pokemon.AddPokemon)
api.delete('/pokemon/:id', pokemon.deletePokemon)
api.put('/pokemon/:id', pokemon.updatePokemon)

//api /login

api.post('/login', user.Login)

//optional
api.get('/pokemon/entrenador/:id', pokemon.getPokemonAndEntrenado)


module.exports = api