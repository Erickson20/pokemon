

'use strict'

const Pokemon = require('../models/pokemon');
const Entrenador = require('../models/entrenador')



function getPokemon(req, res) {

    let id = req.params.id
    
    Pokemon.findById(id, (err, pokemon) =>{
        if(err) return res.send({error: err})
        if(!id) return res.send({menssage: 'no existe este entrenador'});
        return res.send({pokemon: pokemon})
    })
    
}

function getEndPokemons(req, res) {
    let poke = Pokemon.find().skip(Pokemon.count() -2)
    return res.send({pokemon: Pokemon.count()});
    
}
//optional
function getPokemonAndEntrenado(req, res){
    let id = req.params.id
    Pokemon.findById(id, (err, pokemons) => {
        if(err) return res.send({error: err})
            Entrenador.populate(pokemons, {path: "Entrenador"},function(err, pokemons){
                if(err) return res.send({error: err})
                return res.send(pokemons);
        }); 
    });

}

function getAllPokemons(req, res) {

    Pokemon.find({}, (err, pokemons) =>{
        if(err) return res.send({error: err})
        return res.send({Pokemon: pokemons})
    })
    
}

function AddPokemon(req, res) {
    let pokemon = new Pokemon({
        name: req.body.name,
        type: req.body.type,
        img: req.body.img,
        level: req.body.level,
        DateCatch: req.body.catch,
        Entrenador: req.body.entrenador
    })

        pokemon.save((err) =>{
            if(err) return res.send({error: err})

            return res.send({pokemon: pokemon})
        })

    
}


function deletePokemon(req, res) {
    let id = req.params.id
    
    Pokemon.findById(id, (err, pokemon) =>{
        if(err) return res.send({error: err})
        if(!id) return res.send({menssage: 'no existe este pokemon'});
        pokemon.remove( (err) =>{
            if(err) return res.send({error: err})
            return res.send({menssage: 'se elimino el pokemon con el id: '+id})
        })
    })
    
}

function updatePokemon(req, res) {

    let id = req.params.id
    let body = req.body

    Pokemon.findByIdAndUpdate(id, body, (err, pokemon) =>{
        if(err) return res.send({error: err})
        if(!id) return res.send({menssage: 'no existe este pokemon'});
        return res.send({menssage: 'se actualizo el pokemon'})
    })
    
}

module.exports = {
    getAllPokemons,
    getPokemon,
    deletePokemon,
    updatePokemon,
    AddPokemon,
    getPokemonAndEntrenado,
    getEndPokemons
}