'use strict'

const Entrenador = require('../models/entrenador')

const service = require('../services/token')




function Login(req, res) {
    Entrenador.findOne({user: req.body.user}, (err, user) => {
        if(err) return res.send({error: err});
        if(!user) return res.send({menssage: 'el user no existe'});
        return res.status(200).send({token: service.createToken(user)});
    });
    
}

module.exports = {
    Login
}