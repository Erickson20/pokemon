'use strict'

const Entrenador = require('../models/entrenador')

function addEntrenador(req, res){

    let entrenador = new Entrenador({
        user: req.body.user,
        name: req.body.name,
        password: req.body.password,
        img: req.body.img,
        Date: req.body.date,
    })

        entrenador.save((err) =>{
            if(err) return res.send({error: err})

            return res.send({entrenador: entrenador})
        })

        
}

function getEntrenador(req, res){
    let id = req.params.id
    
    Entrenador.findById(id, (err, entrenador) =>{
        if(err) return res.send({error: err})
        if(!id) return res.send({menssage: 'no existe este entrenador'});
        return res.send({entrenador: entrenador})
    })
}


function getAllEntrenador(req, res){
    Entrenador.find({}, (err, entrenadores) =>{
        if(err) return res.send({error: err})
        return res.send({Entrenadores: entrenadores})
    })
}


function deleteEntrenador(req, res){
    let id = req.params.id
    
    Entrenador.findById(id, (err, entrenador) =>{
        if(err) return res.send({error: err})
        if(!id) return res.send({menssage: 'no existe este entrenador'});
        entrenador.remove( (err) =>{
            if(err) return res.send({error: err})
            return res.send({menssage: 'se elimino el entrenador con el id: '+id})
        })
    })
}

function updateEntrenador(req, res){
    let id = req.params.id
    let body = req.body

    Entrenador.findByIdAndUpdate(id, body, (err, entrenador) =>{
        if(err) return res.send({error: err})
        if(!id) return res.send({menssage: 'no existe este entrenador'});
        return res.send({menssage: 'se actualizo el entrenador'})
    })
}


module.exports = {
    addEntrenador,
    getEntrenador,
    getAllEntrenador,
    deleteEntrenador,
    updateEntrenador
}